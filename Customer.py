import random
import pandas as pd
import matplotlib.pyplot as plt


#### CREATE CUSTOMER
class Customer():
    nbCustomer=0
    'class customer defines a customer of the coffee bar by given an id number to customer'
    def __init__(self):
        self.Id=Customer.nbCustomer+1
        Customer.nbCustomer=Customer.nbCustomer+1

    def BuyDrinks(self,TimeIndex,PbData):
        pb=PbData.iloc[TimeIndex]
        Time=pb.Time_bis
        pbCoffe=pb.coffe
        pbFrappucino=pb.frappucino
        pbMilkshake = pb.milkshare
        pbSoda = pb.soda
        pbTea = pb.tea
        pbWater = pb.water
        ##The price for drinks is milkshake = 5, frappucino = 4, water = 2 and the rest 3.
        r=random.uniform(0,1)
        if r<pbCoffe:
            Drink="Coffe"
            Price=3
        elif r<(pbCoffe+pbFrappucino):
            Drink="Frappucino"
            Price=4
        elif r<(pbCoffe+pbFrappucino+pbMilkshake):
            Drink="Milkshake"
            Price=5
        elif r<(pbCoffe+pbFrappucino+pbMilkshake+pbSoda):
            Drink="Soda"
            Price=3
        elif r<(pbCoffe+pbFrappucino+pbMilkshake+pbSoda+pbTea):
            Drink="Tea"
            Price=3
        else:
            Drink="Water"
            Price=2
        return Time,Drink,Price

    def BuyFoods(self,TimeIndex,PbData):
        pb=PbData.iloc[TimeIndex]
        Time=pb.Time_bis
        pbCookie=pb.cookie
        pbMuffin = pb.muffin
        pbNothing = pb.nothing
        pbPie = pb.pie
        pbSandwich = pb.sandwich
        ##The price for the food is sandwich = 5, cookie = 2 and the rest 3.
        r=random.uniform(0,1)
        if r<pbCookie:
            Food="Cookie"
            Price=2
        elif r<(pbCookie+pbMuffin):
            Food="Muffin"
            Price=3
        elif r<(pbCookie+pbMuffin+pbNothing):
            Food="Nothing"
            Price=0
        elif r<(pbCookie+pbMuffin+pbNothing+pbPie):
            Food="Pie"
            Price=3
        else:
            Food="Sandwich"
            Price=5
        return Time,Food,Price



class OneTime(Customer):
    ' class OneTime defines for the customer only come once'
    def __init__(self):
        Customer.__init__(self)
        self.budget=100


class Returning(Customer):
    MaxPrice = 10
    def __init__(self):
        Customer.__init__(self)
        self.EnoughBudget=True

    def RenewBudget(self,Price):
        if self.budget<Returning.MaxPrice:
            #If a returning customer is no longer able to buy the most expensive food AND drink
            #it should no longer be possible to randomly draw him/her from the pool of 1000 returning customers
            self.EnoughBudget=False
        else:
            self.budget=self.budget-Price

class RegularOneTime(OneTime):
    def __init__(self):
        OneTime.__init__(self)

class TripAdvisor(OneTime):
    def __init__(self):
        # a customer from the TripAdvisor gives a random tip between 1 to 10 euros
        OneTime.__init__(self)
        self.tips=random.randint(1,10)

class RegularReturning(Returning):
    def __init__(self):
        Returning.__init__(self)
        self.budget=250

class Hipsters(Returning):
    def __init__(self):
        Returning.__init__(self)
        self.budget=500





# test code
"""
toto1=TripAdvisor()
print(toto1.budget)
print(toto1.tips)

toto2=RegularOneTime()
print(toto2.budget)

toto3=Hipsters()
print(toto3.Id)
print(toto3.budget)

toto4=RegularReturning()
print(toto4.budget)
"""

pbFoods=pd.read_table('PB_Foods.csv',sep=',',names=["Time_bis","cookie","muffin","nothing","pie","sandwich"],header=0)
pbDrinks=pd.read_table('PB_Drinks.csv',sep=',',names=["Time_bis","coffe","frappucino","milkshare","soda","tea","water"],header=0)

# test functions BuyDrinks and BuyFoods
#Hour,Drink,Price=toto1.BuyDrinks(1,pbDrinks)
#print(Hour,Drink,Price)

#Hour,Food,Price=toto1.BuyFoods(40,pbFoods)
#print(Hour,Food,Price)

#test function EnoughBudget for returning customers
#print(toto4.RenewBudget(800))


# Initialization of 1000 returning customers
ReturningCustomerList=[]
nbReturnCustomer=1000
pbHipster=1/3
for i in range(int(pbHipster*nbReturnCustomer)):
    ReturningCustomerList.append(Hipsters())
for i in range(nbReturnCustomer-int(pbHipster*nbReturnCustomer)):
    ReturningCustomerList.append(RegularReturning())

# simulation
from datetime import date,timedelta

d1 = date(2013, 1, 1)  # start date
d2 = date(2017, 12, 31)  # end date
DeltaDate=d2-d1
HourList=pbDrinks.Time_bis

Simulation=[]
day=d1
MaxFoodPrice=5
MaxDrinkPrice=5

print("The following code may takes several minutes plesas wait")

for d in range(DeltaDate.days):
    day=day+timedelta(1)
    for h in range(0,len(HourList)):
        CustomerReturning=(random.random()<=0.2)
        tips=0
        if CustomerReturning:
            FindReturningCustomer=False
            while not FindReturningCustomer:
                # randomly choose a returning customer from the returning customer's list
                CustomerId=random.randint(0,nbReturnCustomer-1)
                # judge if there is enough budget rest in customer's account
                # if not enough budget randomly choose another customer
                if ReturningCustomerList[CustomerId].EnoughBudget:
                    FindReturningCustomer=True
            CustomerComing=ReturningCustomerList[CustomerId]
            TimeDrink, Drink, DrinkPrice = CustomerComing.BuyDrinks(h, pbDrinks)
            CustomerComing.RenewBudget(DrinkPrice)
            TimeFood, Food, FoodPrice = CustomerComing.BuyFoods(h, pbFoods)
            CustomerComing.RenewBudget(FoodPrice)
        else:
            # among the onetime customers 1/10 are coming because of tripadvisor
            aux=random.random()
            if aux<0.1:
                CustomerComing=TripAdvisor()
                tips=CustomerComing.tips
            else:
                CustomerComing=RegularOneTime()
        TimeDrink,Drink,DrinkPrice=CustomerComing.BuyDrinks(h,pbDrinks)
        TimeFood,Food,FoodPrice=CustomerComing.BuyFoods(h,pbFoods)
        # store the information in a list
        BuyInformation=[str(day),TimeDrink,CustomerComing.Id,Drink,DrinkPrice,Food,FoodPrice,tips,CustomerComing.budget]
        Simulation.append(BuyInformation)


SimulationData=pd.DataFrame(Simulation,columns=['Day','Time','Id','Drink','DrinkPrice','Food','FoodPrice','Tips','BalanceBudget'])
SimulationData.to_csv('Simulation.csv')
SimulationData["Drink"]=pd.Categorical(SimulationData["Drink"],ordered=False)
SimulationData["Food"]=pd.Categorical(SimulationData["Food"],ordered=False)
SimulationData["Id"]=pd.Categorical(SimulationData["Id"],ordered=False)
SimulationData.head(n=5)

SimulationData.describe()

plt.figure(1)
SimulationData['Drink'].value_counts()
SimulationData['Drink'].value_counts().plot(kind="bar")
plt.show()

plt.figure(2)
SimulationData['Food'].value_counts()
SimulationData['Food'].value_counts().plot(kind="bar")
plt.show()

DailyDrink=SimulationData.groupby('Time')['DrinkPrice'].sum()
DailyFood=SimulationData.groupby('Time')['FoodPrice'].sum()
DailyTips=SimulationData.groupby('Time')['Tips'].sum()
DailyRevenue=(DailyDrink+DailyFood+DailyTips)/(5*365)
# the following graph shows the daily avenue varying on time
plt.figure(3)
DailyRevenue.plot()
plt.show()

