# part1 determine probabilities
#--------------------------------
# import the package pandas used for manipulation and analysis of data
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

data = pd.read_table('Coffeebar_2013-2017.csv',sep=';', names=["Time","Customer","Drinks","Food"],header=0)
data["Food"]=data["Food"].replace(np.nan,"nothing")
print(data.dtypes)
data["Drinks"]=pd.Categorical(data["Drinks"],ordered=False)
data["Food"]=pd.Categorical(data["Food"],ordered=False)
print("######## types of varaibles ##############")
print(data.dtypes)

#Question 1
#What food and drinks are sold by the coffee bar? How many unique customers did the bar have?
# using data.describe, we find that there are 247989 unique customers
print("######## a summary of the dataset ##############")
print(data.describe())
# value_count will return all the possibles categories of a category variable
print("########## categoriess of a category  variable###########")
print(data["Drinks"].value_counts())
print("__________________________")
print(data["Food"].value_counts())

#Question 2
#a bar plot of the total amount of sold foods (plot1) and drinks (plot2) over the five years
plt.figure(1)
data["Drinks"].value_counts().plot(kind='bar')
plt.xlabel("Type of drinks")
plt.ylabel("Frenquency")
plt.title("bar plot of drinks")
plt.show()

plt.figure(2)
data["Food"].value_counts().plot(kind='bar')
plt.xlabel("Type of foods")
plt.ylabel("Frenquency")
plt.title("bar plot of foods")
plt.show()

plt.figure(3)
# Mosaic plot
from statsmodels.graphics.mosaicplot import mosaic
mosaic(data,["Drinks","Food"])
plt.show()

#Question 3
# to run this part you need to close the figures window before
#Determine the average that a customer buys a certain food or drink at any given time
list = (data["Time"])
Time_bis=[]
for i in list:
    Time_bis.append(i[-8:-3])
data["Time_bis"]=Time_bis
print(data.head())

# create a contingency table
print("The following codes may take several minutes, please wait patiently")
table_drinks=pd.crosstab(data["Time_bis"],data["Drinks"],normalize='index')
print(table_drinks)


table_foods=pd.crosstab(data["Time_bis"],data["Food"],normalize='index')
print(table_foods)

table_foods.to_csv('PB_Foods.csv')
table_drinks.to_csv('PB_Drinks.csv')

